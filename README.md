# image-content-filtration-serve

## Broad intro

Repo adapting https://github.com/htried/Image-Content-Filtration for deployment on WMF LiftWing architecture.

Based on https://github.com/AikoChou/kserve-example/tree/main/alexnet-model.

Note that `dog.jpeg` is the source image for `input_sfw.json`. The source image for `input_nsfw.json` is available on Wikimedia Commons at [this link](https://commons.wikimedia.org/wiki/File:Doggystyle_Fucking.jpg). Be warned: it is *very 
**very*** NSFW.

## Setup

(This setup routine is almost entirely based on the alexnet example linked above)

1. Install [Docker](https://www.docker.com/products/docker-desktop/)
2. Clone this repository to your local
3. Make sure that the model weights (`hal-retraining_run_3.h5`) is in `models/`

### Build the Docker image

Change your directory to `image-content-filtration-serve`
```
cd image-content-filtration-serve/
```

Make sure your Docker is running locally!

Run the following command to build the Docker image
```
docker build -t <docker-tag> -f Dockerfile .
```

After the building is finished, you may want to check if the image is in your local environment
```
docker image ls
``` 

### Deploy Locally and Test

Launch the docker image built from last step
```
docker run --rm -p 8080:8080 -v `pwd`/models:/mnt/models <docker-tag>
```
* `-p 8080:8080` exposes port 8080 of the container to port 8080 of the host
* `-v "$(pwd)"/models:/mnt/models` mounts `models` directory to `/mnt/models`, so kserve can access the models in the container.

If everything goes fine, you should see logs reporting from kserve
```
[I 220725 09:06:00 kfserver:150] Registering model: alexnet-model
[I 220725 09:06:00 kfserver:120] Setting asyncio max_workers as 8
[I 220725 09:06:00 kfserver:127] Listening on port 8080
[I 220725 09:06:00 kfserver:129] Will fork 1 workers
```

Now we are ready to test the model server!

Open a new terminal, change dir to `image-content-filtration-serve/` and run the following command to send a test inference request
```
curl localhost:8080/v1/models/image-content-filtration-model:predict -d @<input_file>.json
```
The `input_(n)sfw.json` files follow the [Tensorflow V1 HTTP API](https://www.tensorflow.org/tfx/serving/api_rest#encoding_binary_values) for encoding image bytes. We encoded a test image `dog.jpeg` to a base64 string and encapsulated it in `input_sfw.json`. A NSFW image from commons was encoded as a base64 string and encapsulated in `input_nsfw.json`.

If everything goes fine, you should see the following output for `input_sfw.json`:
```
{"prob_nsfw": 6.397998884161149e-12, "prob_sfw": 1.0}
```
and the following output for `input_nsfw.json`:
```
{"prob_nsfw": 0.9999992847442627, "prob_sfw": 7.47561784919526e-07}
```
